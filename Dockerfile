FROM python:3.7
RUN apt-get install openssl
WORKDIR /usr/local/bin
COPY myserver.py .
CMD ["python","myserver.py"]
